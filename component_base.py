# -*- coding: UTF-8 -*-
# ------------------------------------------------------------------------------
# 
# ------------------------------------------------------------------------------

import zope.interface as interface
from zope.component import queryUtility

from .debug_interfaces import ILogger, IDebug


# ------------------------------------------------------------------------------
# DBG
# ------------------------------------------------------------------------------
class ScvComponentBase(object):

    def __init__(self):
        logger_compo = queryUtility(ILogger, name=self.__class__.__name__) \
            or queryUtility(ILogger, default=None)
        if logger_compo:
            self.logger = logger_compo(source=self)
        else:
            self.logger = DummyLogger

        self.debug = queryUtility(IDebug, name=self.__class__.__name__) \
            or queryUtility(IDebug, default=None)


# ------------------------------------------------------------------------------
@interface.provider(ILogger)
class DummyLogger(object):

    # INTERFACED #
    @classmethod
    def debug(cls, fmt, **kwargs):
        pass
