# -*- coding: UTF-8 -*-
# ------------------------------------------------------------------------------
#
# ------------------------------------------------------------------------------

import time
from datetime import datetime
from collections import defaultdict

import zope.interface as interface
from pympler.asizeof import asizeof

from .debug_interfaces import ILogger, IDebug


# ------------------------------------------------------------------------------
# DBG (utilities, not components)
# ------------------------------------------------------------------------------
@interface.implementer(ILogger)
class ScvSimpleLogger(object):

    def __init__(self, source=None):
        self.source = source

    # INTERFACED #
    def debug(self, fmt, **kwargs):
        dt = datetime.now().strftime('%H:%M:%S:%f')[:-3]
        print(dt, self.source.__class__.__name__, '::', fmt, **kwargs)


@interface.provider(IDebug)
class ScvDebug(object):

    SCVDEV = ({
        'SCV_DEBUG_LOG',  # not working yet
        # 'SCV_PROFILE_NET',
        # 'SCV_PROFILE_DRAW',
        # 'SCV_PROFILE_TICK',
        # 'SCV_MEMORY',
        # 'SCV_MEMORY_FILE_DUMP',
    })

    # --------------------------------------
    # PROFILER
    class SCVPROFILE1(object):
        data = defaultdict(lambda: {'start': None, 'sum': 0, 'len': 0})

        @classmethod
        def start(cls, sid):
            cls.data[sid]['start'] = time.time()

        @staticmethod
        def start_for_remote(sid=None):
            return time.time()

        @classmethod
        def measure(cls, sid, start=None):
            pval = int(round((time.time() - (start or cls.data[sid]['start'])) * 1000))
            cls.data[sid]['sum'] += pval
            cls.data[sid]['len'] += 1
            return pval

        @classmethod
        def measure_ignoring_zero(cls, sid, start=None):
            pval = int(round((time.time() - (start or cls.data[sid]['start'])) * 1000))
            if pval:
                cls.data[sid]['sum'] += pval
                cls.data[sid]['len'] += 1
            return pval

        @classmethod
        def average(cls, sid):
            return round(cls.data[sid]['sum'] / cls.data[sid]['len'], 2)

    # --------------------------------------
    # MEMORY
    @staticmethod
    def SCVMEMORY1(*objs, **opts):
        asizeof(*objs, **opts)
