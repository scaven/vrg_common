# -*- coding: UTF-8 -*-
# ------------------------------------------------------------------------------
# 
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# The most basic data holder
# ------------------------------------------------------------------------------
class SimpleVolume(object):
    __slots__ = '_volume_name', '__dict__'

    def __init__(self, _volume_name='SimpleVolume', _volume_dict=None, **kwargs):
        self._volume_name = _volume_name

        self.__dict__.update(kwargs)
        if isinstance(_volume_dict, dict):
            self.__dict__.update(_volume_dict)
        elif _volume_dict:
            raise TypeError('SimpleVolume supports only dict instances')

    def __contains__(self, item):
        return item in self.__dict__

    def __str__(self):
        name = self.__class__.__name__ if self.__class__.__name__ != 'SimpleVolume' else self._volume_name
        return '{classname}({dict})'.format(classname=name, dict=self.__dict__)


# ------------------------------------------------------------------------------
# Advanced data holder
#  + recursuve serialization
#  + ability for callbacks on changed data
# ------------------------------------------------------------------------------
class ComplexVolume(SimpleVolume):
    def __init__(self, **kwargs):
        super(ComplexVolume, self).__init__(**kwargs)

    # TODO on_changed callbacks caps
