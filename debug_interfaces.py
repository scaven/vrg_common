#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# 
# ------------------------------------------------------------------------------

import zope.interface as interface


# ------------------------------------------------------------------------------
class ILogger(interface.Interface):

    def debug(fmt, **kwargs):
        """ """


class IDebug(interface.Interface):
    pass
