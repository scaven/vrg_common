# -*- coding: UTF-8 -*-
# ------------------------------------------------------------------------------
# 
# ------------------------------------------------------------------------------
import sys
if __name__ == '__main__':
    sys.path.insert(0, '..')

import zope.interface as interface
from zope.interface import verify


# ------------------------------------------------------------------------------
print()
print('#'*25)
print('##### COMMON #####')
# ------------------------------------------------------------------------------
from common.component_base import DummyLogger
from common.debug_interfaces import ILogger, IDebug
from common.debug_utils import ScvSimpleLogger, ScvDebug

def scvtest_debug_interfaces():
    print('# scvtest_debug_interfaces')
    interface.verify.verifyObject(ILogger, DummyLogger)
    interface.verify.verifyClass(ILogger, ScvSimpleLogger)
    interface.verify.verifyObject(IDebug, ScvDebug)

scvtest_debug_interfaces()
